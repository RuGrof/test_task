var RACQUET = {
    width: 150,
    height: 20,
    speed: 300
}
var HOST_GAME = '10.101.248.11';
var HOST_PORT = '3000';

var socket; //Global for Socket.io

var TennisGame = {
    createInput: function (x, y, contextc) {
        var bmd = contextc.add.bitmapData(400, 50);
        var myInput = contextc.game.add.sprite(x, y, bmd);

        myInput.canvasInput = new CanvasInput({
            canvas: bmd.canvas,
            fontSize: 30,
            fontFamily: 'Arial',
            fontColor: '#212121',
            fontWeight: 'bold',
            width: 400,
            padding: 8,
            borderWidth: 1,
            borderColor: '#000',
            borderRadius: 3,
            boxShadow: '1px 1px 0px #fff',
            innerShadow: '0px 0px 5px rgba(0, 0, 0, 0.5)',
            placeHolder: 'Enter message here...'
        });
        myInput.inputEnabled = true;
        myInput.input.useHandCursor = true;
        myInput.events.onInputUp.add(this.inputFocus, contextc);

        return myInput;
    },
    inputFocus: function (sprite) {
        sprite.canvasInput.focus();
    }
};

TennisGame.bootState = function (game) {

};

TennisGame.bootState.prototype = {
    create: function () {
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.stage.disableVisibilityChange=true;

        this.game.state.start('mainMenu');

    }
}


TennisGame.mainMenuState = function (game) {
    this.createGameButton;
    this.joinGameButton;
};

TennisGame.mainMenuState.prototype = {
    create: function () {
        this.createGameButton = game.add.text(this.game.world.centerX, this.game.world.centerY - 65, "Create Game", { font: "65px Arial", fill: "#009688", align: "center" });
        this.createGameButton.anchor.set(0.5);
        this.createGameButton.inputEnabled = true;
        this.createGameButton.input.useHandCursor = true;
        this.createGameButton.events.onInputUp.add(this.createGame, this);

        this.joinGameButton = game.add.text(this.game.world.centerX, this.game.world.centerY + 65, "Join Game", { font: "65px Arial", fill: "#009688", align: "center" });
        this.joinGameButton.anchor.set(0.5);
        this.joinGameButton.inputEnabled = true;
        this.joinGameButton.input.useHandCursor = true;
        this.joinGameButton.events.onInputUp.add(this.joinGame, this);
    },

    createGame: function () {
        this.game.TennisMaster = true;
        $.ajax({
            url: "http://"+HOST_GAME+":"+HOST_PORT+"/CreateGame",
            method: "GET",
            dataType: 'json',
            context:this
        }).done(function (data) {
            var doneCtx = this;
            
            console.log(data.Game);
            
            this.game.gameNumber = data.Game._id;
            socket = io.connect(data.host.host);
            //console.log(socket);
            socket.on('welcom',function () {
                var gameNumberInput;
                var joinGameButton;
                
                socket.emit('joinGame', { gameNumber: data.Game._id , TennisMaster : true });
                
                doneCtx.joinGameButton.destroy();
                doneCtx.createGameButton.destroy();
                
                gameNumberInput = TennisGame.createInput(game.world.centerX, game.world.centerY - 100, doneCtx);
                gameNumberInput.anchor.set(0.5);
                gameNumberInput.canvasInput.placeHolder('');
                gameNumberInput.canvasInput.value(data.Game._id);
                gameNumberInput.canvasInput.selectText();
                gameNumberInput.canvasInput.onsubmit(function () {
                   document.execCommand('copy');
                });
                
                joinGameButton = game.add.text(game.world.centerX, game.world.centerY + 65, "Click to copy, \n Game Number \n wait...", { font: "65px Arial", fill: "#009688", align: "center" });
                joinGameButton.anchor.set(0.5);
                joinGameButton.inputEnabled = true;
                joinGameButton.input.useHandCursor = true;
                joinGameButton.events.onInputUp.add(function(){
                    document.execCommand('copy');
                });
                
                
            });
            //------- After join Secon player, start game
            socket.on('startGame',function(){
                game.state.start('play');
            });

            //console.log('Ajax done',data)
        }).fail(function () {
            console.log('Ajax fail')
        });

        console.log('create game');
    },

    joinGame: function () {
        this.game.TennisMaster = false;
        game.state.start('networkJoinMenu');
    }

}

TennisGame.networkJoinMenuState = function (game) {
    this.gameNumberInput;
};

TennisGame.networkJoinMenuState.prototype = {
    create: function () {
        var ctx = this;
        this.gameNumberInput = TennisGame.createInput(this.game.world.centerX, game.world.centerY - 100, this);
        this.gameNumberInput.anchor.set(0.5);
        this.gameNumberInput.canvasInput.placeHolder('Enter Game Number');
        this.gameNumberInput.canvasInput.value('');
        this.gameNumberInput.canvasInput.onsubmit(function () {
            ctx.setGameNumber();
        });

        this.connectButton = game.add.text(game.world.centerX, game.world.centerY, "Connect", { font: "65px Arial", fill: "#009688", align: "center" });
        this.connectButton.anchor.set(0.5);
        this.connectButton.inputEnabled = true;
        this.connectButton.input.useHandCursor = true;
        this.connectButton.events.onInputUp.add(this.setGameNumber, this);
    },
    setGameNumber: function () {
        this.game.gameNumber = this.gameNumberInput.canvasInput.value();
        $.ajax({
            url: "http://"+HOST_GAME+":"+HOST_PORT+"/getgame?gameNumber=" + this.game.gameNumber,
            method: "GET",
            dataType: 'json',
            context: this
        }).done(function (data) {
            console.log(data);
            socket = io.connect(data.Game.host);
            socket.on('welcom', function () {
                socket.emit('joinGame', { gameNumber: data.Game._id, TennisMaster: false });    
            });
            socket.on('startGame',function(){
                console.log('startGame');
                
                game.state.start('play');
            });

        }).fail(function () {
            console.log('Ajax fail')
        });

    }
}

TennisGame.playState = function (game) {
    this.racquetPlayerMaster;
    this.racquetPlayerRemote;

    this.textStarted;

    this.ball;
    this.ballStarted;

    this.startX;
    this.startY;

    this.masterScoreText;
    this.remoteScoreText;


};

TennisGame.playState.prototype = {
    create: function () {
        var ctxPlayState = this;
        this.game.masterScore = 0;
        this.game.remoteScore = 0;

        this.ballStarted = false;
        this.game.physics.arcade.checkCollision.down = false;
        this.game.physics.arcade.checkCollision.up = false;
        var bmd = this.game.add.bitmapData(RACQUET.width, RACQUET.height);


        bmd.ctx.beginPath();
        bmd.ctx.rect(0, 0, RACQUET.width, RACQUET.height);
        bmd.ctx.fillStyle = '#009688';
        bmd.ctx.fill();

        this.racquetPlayerMaster = this.game.add.sprite(this.game.world.centerX, this.game.world.height - 10, bmd);
        this.racquetPlayerMaster.anchor.set(0.5);

        this.game.physics.arcade.enable(this.racquetPlayerMaster);

        this.racquetPlayerMaster.body.collideWorldBounds = true;
        this.racquetPlayerMaster.body.bounce.set(1);
        this.racquetPlayerMaster.body.immovable = true;
        //----------------------------------
        this.racquetPlayerRemote = this.game.add.sprite(this.game.world.centerX, 10, bmd);
        this.racquetPlayerRemote.anchor.set(0.5);

        this.game.physics.arcade.enable(this.racquetPlayerRemote);

        this.racquetPlayerRemote.body.collideWorldBounds = true;
        this.racquetPlayerRemote.body.bounce.set(1);
        this.racquetPlayerRemote.body.immovable = true;
        ///----------------------------------
        //if (this.game.TennisMaster) {
        if(true){
            this.startX = this.racquetPlayerMaster.body.center.x - RACQUET.width / 2;
            this.startY = this.racquetPlayerMaster.body.center.y - RACQUET.height - 10;
        } else {
            this.startX = this.racquetPlayerRemote.body.center.x - RACQUET.width / 2;
            this.startY = this.racquetPlayerRemote.body.center.y + RACQUET.height / 2;
        }
        this.ball = this.game.add.sprite(this.startX, this.startY, 'ballImage');

        this.ball.anchor.set(0.5);
        this.ball.checkWorldBounds = true;
        if (this.game.TennisMaster) {
            this.game.physics.arcade.enable(this.ball);

            this.ball.body.collideWorldBounds = true;
            this.ball.body.bounce.set(1);

            this.ball.events.onOutOfBounds.add(this.ballLost, this);
            
            //---------------------------
            this.textStarted = game.add.text(game.world.centerX, game.world.centerY, "aim and click to start", { font: "65px Arial", fill: "#ff0044", align: "center" });
            this.textStarted.anchor.setTo(0.5, 0.5);
            //---------------------------
            this.game.input.onUp.add(this.startBall, this);
        }
        //-------Score----------
        this.masterScoreText = game.add.text(game.world.centerX, game.world.centerY - 75, this.game.masterScore.toString(), { font: "65px Arial", fill: "#ff0044", align: "center" });
        this.masterScoreText.anchor.setTo(0.5, 0.5);

        this.remoteScoreText = game.add.text(game.world.centerX, game.world.centerY + 75, this.game.remoteScore.toString(), { font: "65px Arial", fill: "#ff0044", align: "center" });
        this.remoteScoreText.anchor.setTo(0.5, 0.5);
        
        //------- Soccet Event --------
        if (this.game.TennisMaster) {
            //--------- Master event ------- 
            socket.on('moving',function(data){
                    ctxPlayState.racquetPlayerRemote.x = data.racquetPlayerRemote;
                });
                
            socket.on('endGame',function(data){
                
                    socket.disconnect();
                    
                    game.state.start('endGame');                    
                });
        } else {
            //--------- Remote Event -----
            socket.on('moving', function (data) {
                ctxPlayState.racquetPlayerMaster.x = data.racquetPlayerMaster;
                ctxPlayState.ball.x = data.Ball.x;
                ctxPlayState.ball.y = data.Ball.y;
            });

            socket.on('startBall', function () {
                ctxPlayState.startBallRemote();
            });

            socket.on('ballLost', function (data) {
                ctxPlayState.game.masterScore = data.masterScore;
                ctxPlayState.game.remoteScore = data.remoteScore;
                ctxPlayState.ballLostRemote();
            });

            socket.on('endGame', function (data) {
                ctxPlayState.game.masterScore = data.masterScore;
                ctxPlayState.game.remoteScore = data.remoteScore;
                
                socket.disconnect();
                
                game.state.start('endGame');
            });
        }
        
    },

    preload: function () {
        this.game.load.image('ballImage', 'img/ball.png');
    },

    render: function () {
        this.game.debug.body(this.racquetPlayerMaster);
        this.game.debug.body(this.racquetPlayerRemote);
        this.game.debug.body(this.ball);
    },

    update: function () {
        this.game.physics.arcade.collide(this.ball, this.racquetPlayerMaster);
        this.game.physics.arcade.collide(this.ball, this.racquetPlayerRemote);
        if (this.ballStarted) {
            if (this.game.input.x > RACQUET.width / 2 - 10 && this.game.input.x < 810 - RACQUET.width / 2) {
                if (this.game.TennisMaster) {
                    
                    this.racquetPlayerMaster.x = this.game.input.x;
                    
                    socket.emit('move',{
                        gameNumber : this.game.gameNumber,
                        TennisMaster : true,
                        Ball : {x:this.ball.x, y:this.ball.y},
                        racquetPlayerMaster : this.racquetPlayerMaster.x
                        });
                    
                } else {
                    this.racquetPlayerRemote.x = this.game.input.x;
                    socket.emit('move',{
                        gameNumber : this.game.gameNumber,
                        TennisMaster : false,
                        racquetPlayerRemote : this.racquetPlayerRemote.x
                        });
                }
            }
        }
    },

    ballLost: function () {

        //this.ball.y   < 0 top bound
        //              > 0 bottom bound 
        //------- Score --------
        if (this.ball.y < 0) {
            this.game.masterScore++;
        } else {
            this.game.remoteScore++;
        }
        //-------- End Game --------

        if (this.game.masterScore + this.game.remoteScore > 2) {
            socket.emit('endGame',{
                 gameNumber : this.game.gameNumber,
                 score:{
                     masterScore: this.game.masterScore, 
                     remoteScore: this.game.remoteScore
                     }                 
                 });                 
            
        } else {
             socket.emit('ballLost',{
                 gameNumber : this.game.gameNumber,
                 score:{
                     masterScore: this.game.masterScore, 
                     remoteScore: this.game.remoteScore
                     }                 
                 });
            
            this.ball.body.reset(this.startX, this.startY);
            this.ball.x = this.startX;
            this.ball.y = this.startY;
            //------------------------------
            this.ballStarted = false;
            
            //-------------------
            this.racquetPlayerRemote.x = this.game.world.centerX;
            this.racquetPlayerRemote.y = 10;
            
            //--------------------
            this.racquetPlayerMaster.x = this.game.world.centerX;
            this.racquetPlayerMaster.y = this.game.world.height - 10;
            
            //---------------------
            this.textStarted.visible = true;

            console.log(this.ball.y);
            this.masterScoreText = game.add.text(game.world.centerX, game.world.centerY + 75, this.game.masterScore.toString(), { font: "65px Arial", fill: "#ff0044", align: "center" });
            this.masterScoreText.anchor.setTo(0.5, 0.5);

            this.remoteScoreText = game.add.text(game.world.centerX, game.world.centerY - 75, this.game.remoteScore.toString(), { font: "65px Arial", fill: "#ff0044", align: "center" });
            this.remoteScoreText.anchor.setTo(0.5, 0.5);
        }

    },
    
    startBall: function () {

        if (!this.ballStarted) {
            this.ball.body.velocity.x = Math.cos(game.physics.arcade.angleToPointer(this.ball)) * RACQUET.speed;
            this.ball.body.velocity.y = Math.sin(game.physics.arcade.angleToPointer(this.ball)) * RACQUET.speed;
            this.ballStarted = true;

            this.textStarted.visible = false;

            this.masterScoreText.destroy();
            this.remoteScoreText.destroy();
            
        }
         socket.emit('startBall',{
                        gameNumber : this.game.gameNumber
                        });
    },
    
    //--------- Remote
    startBallRemote: function () {
        this.ballStarted = true;
        
        this.masterScoreText.destroy();
        this.remoteScoreText.destroy();
    },
    
    ballLostRemote: function () {

        //------------------------------
        this.ballStarted = false;
        this.ball.x = this.startX;
        this.ball.y = this.startY;    
        //-------------------
        this.racquetPlayerRemote.x = this.game.world.centerX;
        this.racquetPlayerRemote.y = 10;
            
        //--------------------
        this.racquetPlayerMaster.x = this.game.world.centerX;
        this.racquetPlayerMaster.y = this.game.world.height - 10;
            
        //-------------------
        this.masterScoreText = game.add.text(game.world.centerX, game.world.centerY + 75, this.game.masterScore.toString(), { font: "65px Arial", fill: "#ff0044", align: "center" });
        this.masterScoreText.anchor.setTo(0.5, 0.5);

        this.remoteScoreText = game.add.text(game.world.centerX, game.world.centerY - 75, this.game.remoteScore.toString(), { font: "65px Arial", fill: "#ff0044", align: "center" });
        this.remoteScoreText.anchor.setTo(0.5, 0.5);
    },
    //---------- Remote end
    getStartX: function () {
        return this.StartX;
    },
    getStartY: function () {
        return this.StartY;
    }
}

TennisGame.endGameState = function (game) {

    this.scoreText;
    this.playerNameInput;
    this.sumbitButton;

};

TennisGame.endGameState.prototype = {

    create: function () {
        var ctx=this;
        
        if (this.game.TennisMaster) {
            this.scoreText = game.add.text(game.world.centerX, game.world.centerY-150, "You Score " + this.game.masterScore.toString(), { font: "65px Arial", fill: "#ff0044", align: "center" });
        } else {
            this.scoreText = game.add.text(game.world.centerX, game.world.centerY-150, "You Score " + this.game.remoteScore.toString(), { font: "65px Arial", fill: "#ff0044", align: "center" });
        }
        this.scoreText.anchor.setTo(0.5, 0.5);
        
        //--------------------------------
        this.playerNameInput = TennisGame.createInput(this.game.world.centerX, game.world.centerY , this);
        this.playerNameInput.anchor.set(0.5);
        this.playerNameInput.canvasInput.placeHolder('Enter You Name');
        this.playerNameInput.canvasInput.value('');
        this.playerNameInput.canvasInput.onsubmit(function () {
            ctx.submitNumber();
        });

        this.sumbitButton = game.add.text(game.world.centerX, game.world.centerY+100, "Submit", { font: "65px Arial", fill: "#009688", align: "center" });
        this.sumbitButton.anchor.set(0.5);
        this.sumbitButton.inputEnabled = true;
        this.sumbitButton.input.useHandCursor = true;
        this.sumbitButton.events.onInputUp.add(this.submitNumber, this);
    },
    submitNumber: function () {
        
        var playerName = this.playerNameInput.canvasInput.value();
        
        this.sumbitButton.destroy();
        this.playerNameInput.destroy();
        this.scoreText.setText("Updating you score...");
        $.ajax({
            url: "http://"+HOST_GAME+":"+HOST_PORT+"/submitScore?gameNumber=" + this.game.gameNumber + "&name="+ playerName + "&score=" + (this.game.TennisMaster ? this.game.masterScore : this.game.remoteScore),
            method: "GET",
            dataType: 'json',
            context: this
        }).done(function (data) {
            this.game.PlayerScore = data.score;
            console.log(data);
            game.state.start('scoreBoard');

            //console.log('Ajax done',data)
        }).fail(function () {
            console.log('Ajax fail')
        });
        console.log({ Name: this.playerNameInput.canvasInput.value(), Score: this.game.TennisMaster ? this.game.masterScore : this.game.remoteScore })
       // game.state.start('scoreBoard');
    }
}

TennisGame.scoreBoardState = function (game) {
    this.nextGameButton;
};

TennisGame.scoreBoardState.prototype = {
    create: function () {
        this.scoreDate = [
            { Name: "Test", Score: "23" },
            { Name: "Test2", Score: "43" }
        ]
        
        this.lableTop = this.game.add.text(game.world.centerX, 65, "Top 5 players \n loading...", { font: "65px Arial", fill: "#009688", align: "center" });
        this.lableTop.anchor.set(0.5);
        $.ajax({
            url: "http://"+HOST_GAME+":"+HOST_PORT+"/scoreBoard",
            method: "GET",
            dataType: 'json',
            context: this
        }).done(function (data) {
            this.game.PlayerScore = data.score;
            this.lableTop.setText("Top 5 players");
            
            data.map(function(elem,index){
                
                var temp;
                temp = this.game.add.text(game.world.centerX, 150 + index * 60, elem.name[0] + ":" + elem.score, { font: "65px Arial", fill: "#009688", align: "center" });
                temp.anchor.set(0.5);
                return temp;
            });
            //game.state.start('scoreBoard');

            //console.log('Ajax done',data)
        }).fail(function () {
            console.log('Ajax fail')
        });


        this.lableYouScore = this.game.add.text(game.world.centerX, 500, "You Score " + this.game.PlayerScore, { font: "40px Arial", fill: "#009688", align: "center" });
        this.lableYouScore.anchor.set(0.5);

        this.nextGameButton = game.add.text(game.world.centerX, 545, "Next Game", { font: "40px Arial", fill: "#909688", align: "center" });
        this.nextGameButton.anchor.set(0.5);
        this.nextGameButton.inputEnabled = true;
        this.nextGameButton.input.useHandCursor = true;
        this.nextGameButton.events.onInputUp.add(this.nextGame, this);
            
        

    },
    nextGame: function () {
        game.state.start('mainMenu');
    }
}
var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'gameDiv');

game.state.add('boot', TennisGame.bootState);
game.state.add('mainMenu', TennisGame.mainMenuState);
game.state.add('networkJoinMenu', TennisGame.networkJoinMenuState);
game.state.add('play', TennisGame.playState);
game.state.add('endGame', TennisGame.endGameState);
game.state.add('scoreBoard', TennisGame.scoreBoardState);

game.state.start('boot');