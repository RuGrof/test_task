var mongoose = require('mongoose');
mongoose.connect('mongodb://tennis:tennis@ds047865.mongolab.com:47865/issuetracker');
var db = mongoose.connection;
var mongo = new Promise(function (resolve, reject) {
    db.on('error', function () {
        reject('error connection to Mongo')
    });
    db.once('open', function () {
        //------- Mongoose Schema ------    
        var gameSchema = new mongoose.Schema({
            gameNumber: String,
            finished: { type: Boolean, default: false },
            host: String,
            key: String
        },
            { collection: 'tennisGame' });
        
        var scoreScjema =  new mongoose.Schema({
            name:  { type: [String], index: true },
            score: Number
        },
            { collection: 'tennisScore' });
        //------- Mongoose Schema END------
    
        //------- Mongoose Model ----------
        
        mongoose.model('Game', gameSchema)
        mongoose.model('Score', scoreScjema)
        //------- Mongoose Model END-------
        
        resolve(mongoose);

    });
});
module.exports = mongo;
