/**
 * get workers in array ['host:port',..,'host:port']
 * getmin() return worker {host, key}
 */

'use strict'
function compareWorker (workerA, workerB){
    return workerA.connections - workerB.connections;
};
var LoadBalancer = function(workerArray) {
    var ctx = this;
    ctx.workers = {};
    ctx.workers.array = workerArray.map(function(elem,index){
        let temp = 'k'+index;
        ctx.workers[temp] = elem;
        return {key:temp, connections :0}
    });
    this.workers.min = 'k0';
};
LoadBalancer.prototype.sort = function(){
    this.workers.array.sort(compareWorker);
    this.workers.min = this.workers.array[0].key;
}
LoadBalancer.prototype.getMin = function() {
    return {host: this.workers[this.workers.min], key:this.workers.min};
}
LoadBalancer.prototype.addConnection = function (key){
    for(let i = 0; i < this.workers.array.length; i++){
        if(this.workers.array[i].key === key){
            this.workers.array[i].connections++;
        }
    }
    this.sort();
}
LoadBalancer.prototype.decConnection = function (key){
    for(let i = 0; i < this.workers.array.length; i++){
        if(this.workers.array[i].key === key){
            this.workers.array[i].connections--;
        }
    }
    this.sort();
}
module.exports = LoadBalancer;

