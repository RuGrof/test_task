'use strict'
var http = require('http');
var url = require('url');
var cp = require('child_process');
var server;

var mongo = require('./mongo');

var LoadBalanser = require('./loadbalancer');


const hostname = '10.101.248.11';
const port = 3000;

var loadBalanser = new LoadBalanser(['http://'+hostname+':3001/','http://'+hostname+':3002/']);

mongo.then(mongoose => {
    console.log('Connect to mongo');

    cp.fork('./socket-server.js', ['teset_arg'], { env: { test_port: 3001 } });
    cp.fork('./socket-server.js', ['teset_arg'], { env: { test_port: 3002 } });
    
    
    server = http.createServer(function (req, res) {
        // Set CORS headers
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Request-Method', '*');
        res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST');
        res.setHeader('Access-Control-Allow-Headers', '*');
        res.setHeader('Cache-control', 'no-cache');
        if (req.method === 'OPTIONS') {
            res.writeHead(200);
            res.end();
            return;
        } else {
            res.writeHead(200, { 'Content-Type': 'application/json' });
            switch(url.parse(req.url).pathname){
                case '/CreateGame':
                        
                        //-----load balancer add
                        var hostGame = loadBalanser.getMin();
                        loadBalanser.addConnection(hostGame.key);
                        ///-------------
                        
                        var game = new mongoose.models.Game({gameNumber: "2342", host:hostGame.host, key:hostGame.key});
                        game.save(function (err, savedGame) {
                            if (err) return console.error(err);
                            res.end(JSON.stringify({Game:savedGame,host:hostGame}));
                        })
                    
                break;
                case '/getgame':
                      var gameID =  url.parse(req.url,true).query.gameNumber;
                      
                      mongoose.models.Game.findOne({ '_id': gameID }, function (err, savedGame) {
                          if (err) return console.error(err);
                          res.end(JSON.stringify({Game:savedGame}));
                      })
                      
                break;
                case '/submitScore':
                    var query = url.parse(req.url, true).query;
                    mongoose.models.Game.findOne({ '_id': query.gameNumber }, function (err, savedGame) {
                        console.log(savedGame);
                        if (err) return console.error(err);
                        if(savedGame){
                            if (!savedGame.finished) {
                                //----- load balancer dec
                                loadBalanser.decConnection(savedGame.key);
                                savedGame.finished = true;
                                savedGame.save(function (err) {
                                    if (err) return handleError(err);
                                });
                            }
                        }else{
                            res.end(JSON.stringify({error:'bad Game Number'}));
                        }

                    });
                    console.log(query.name);
                    mongoose.models.Score.findOneAndUpdate({ name: query.name }, { $inc: { score: query.score } }, { upsert: true }, function(err, sumScore){
                        //----- for first submit
                        if(sumScore){
                            sumScore.score += parseInt(query.score);
                            
                        }else{
                            //----- first submit
                            sumScore = {};
                            sumScore.score = parseInt(query.score); 
                        }
                        
                         
                        res.end(JSON.stringify(sumScore));
                    });
                    
                 break;
                 case '/scoreBoard' :
                      mongoose.models.Score.find().sort({ score: -1 }).limit(5).exec(function(err,data){
                          res.end(JSON.stringify(data));
                      });
 
                 break;   
                default:
                     res.end(JSON.stringify({error:'Chek URL'}));
                break;
            }
        }
    
        // ...
    }).listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}/`);
    });
},
    error => {
        console.log(error);
    });
