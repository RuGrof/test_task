var io = require('socket.io')(process.env.test_port);

io.on('connection', function (socket) {
    io.sockets.in(socket.id).emit('welcom');
    socket.on('joinGame',data => {
       socket.join(data.gameNumber);
       if(!data.TennisMaster){
           io.sockets.in(data.gameNumber).emit('startGame');
       }
       console.log('Game Created', data); 
    });
    
    socket.on('move', data => {
        if (data.TennisMaster) {
            console.log('move');
            socket.broadcast.to(data.gameNumber).emit('moving', {
                Ball: { x: data.Ball.x, y: data.Ball.y },
                racquetPlayerMaster: data.racquetPlayerMaster
            });

        } else {
            socket.broadcast.to(data.gameNumber).emit('moving', {
                racquetPlayerRemote: data.racquetPlayerRemote
            });
        }
    });
    
    socket.on('startBall', data => {
        socket.broadcast.to(data.gameNumber).emit('startBall');
    });
    
    socket.on('ballLost', data => {
        socket.broadcast.to(data.gameNumber).emit('ballLost',data.score);
    });
    socket.on('endGame', data => {
        io.sockets.in(data.gameNumber).emit('endGame',data.score);

    });
    socket.on('disconnect', function () {
        io.emit('user disconnected');
    });
});